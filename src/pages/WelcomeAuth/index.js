import React from 'react';
import { Text, View, Image } from 'react-native';
import Logo from '../../assets/op.png';

const WelcomeAuth = () => {
    return(
        <View>
            <View style={{height: 60, flexDirection: 'row', backgroundColor: 'blue'}}>
                <Image source={Logo} style={{width: 28, height: 40, marginTop: 10, marginLeft: 10}} />
                <Text style={{fontSize: 18, marginTop: 18, marginLeft: 9, color: 'white'}}>Order Pulsa</Text>
            </View>
            <View style={{alignItems: 'center', marginTop: 240}}>
                <Text style={{fontSize: 40, fontWeight: 'bold', color: 'black', }}>Silahkan</Text>
                <Text style={{fontSize: 20, fontWeight: 'bold', color: 'black', }}>cek saldo anda!</Text>
            </View>
        </View>
        
    );
};

export default WelcomeAuth;