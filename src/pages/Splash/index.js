import React, {useEffect} from 'react';
import { Text, View, Image} from 'react-native';
import Logo from '../../assets/op.png';

const Splash = ({navigation}) => {
    useEffect(()=> {
        setTimeout(() => {
             navigation.replace('WelcomeAuth')
        }, 5000)
    })
    return (
        <View style={{justifyContent: 'center', alignItems: 'center', flex: 1, backgroundColor: 'blue'}}>
            <Image source={Logo} style={{width:100, height:142, marginBottom: 20}}></Image>
            <Text style={{fontWeight: 'bold', fontSize: 24, color: 'white'}}>Order Pulsa</Text>
            <Text style={{fontSize: 11, color: 'white'}}>Terkenal Cepat dan Murah</Text>
        </View>
    );
};

export default Splash;